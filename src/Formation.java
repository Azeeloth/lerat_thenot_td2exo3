import java.util.HashMap;

/**
 * Created by RAPHAEL on 21/09/2016.
 */
public class Formation {
    private HashMap<String, Integer> etu;
    int id;

    public Formation(){
        etu = new HashMap<String, Integer>();
    }

    public void supprimerMatiere(String mat) throws MatiereException{
        if(this.etu.containsKey(mat))
            this.etu.remove(mat);
        else
            throw new MatiereException("La matiere n'est pas presente");
    }

    public void ajouterMatiere(String mat, Integer num) throws MatiereException{
        if(!this.etu.containsKey(mat))
            this.etu.put(mat,num);
        else
            throw new MatiereException("La matiere est deja presente");
    }

    public Integer coefficient(String mat){
        if(this.etu.containsKey(mat))
            return this.etu.get(mat);
        else
            return -1;
    }

    public HashMap<String, Integer> getEtu() {
        return etu;
    }
}
