

import java.util.*;

/**
 * Created by thomas on 21/09/16 at 15:43.
 */
public class Etudiant {

    private Identite id;
    private HashMap<String, List<Double>> resultats;
    private Formation formation;


    public Etudiant(String nip, String nom, String prenom, Formation f){
        formation = f;
        id = new Identite(nip,nom,prenom);
        resultats = new HashMap<String, List<Double>>();
    }

    public void ajouterNote(String mat, double note) throws NoteException{
        if(note>= 21 || note <=-1)
            throw new NoteException();
        else{
            if(resultats.containsKey(mat)){
                resultats.get(mat).add(note);
            }else {
                resultats.put(mat, new ArrayList<Double>());
                resultats.get(mat).add(note);
            }
        }
    }

    public double moyenneMatiere(String mat) throws MatiereException{
        if(resultats.containsKey(mat)){
            double total = 0;
            for(int i = 0; i< resultats.get(mat).size(); i++){
                total += resultats.get(mat).get(i);
            }
            return total = total/resultats.get(mat).size();
        }else
            throw new MatiereException("La matiere demandee n'existe pas");
    }

    public double moyenneGenerale() throws MatiereException {
        if(!resultats.isEmpty()){
            Set<String> s = resultats.keySet();
            Iterator<String> it = s.iterator();
            double total = 0;
            int tCoeff = 0;
            while(it.hasNext()){
                String str = it.next();
                tCoeff +=  formation.coefficient(str);
                total = total+(this.moyenneMatiere(str)*formation.coefficient(str));
            }
            return total = total/(tCoeff);

        }else
            return -1;
    }

    public List<Double> getNote(String mat){
        return this.resultats.get(mat);
    }
    public Formation getFormation(){
        return this.formation;
    }
    public Identite getIdentite(){
        return this.id;
    }

}