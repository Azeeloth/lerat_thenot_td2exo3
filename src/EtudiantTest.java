import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by RAPHAEL on 21/09/2016.
 */
public class EtudiantTest {
    Etudiant e;

    @Before
    public void setUp() throws Exception {
        Formation f = new Formation();
        f.ajouterMatiere("Math", 5);
        f.ajouterMatiere("Histoire",3);
        e = new  Etudiant("A","LERAT","Thomas",f);
    }

    @Test
    public void ajouterNote() throws NoteException {
        List<Double> list = new ArrayList<Double>();
        list.add(15.0);
        e.ajouterNote("Math",15.0);
        assertEquals("La notre devrait etre rajoutée",list,e.getNote("Math"));
    }

    @Test(expected = NoteException.class)
    public void AjouterNoteNegative() throws NoteException {
        e.ajouterNote("Math",-10.0);
    }


    @Test
    public void moyenneMatiere() throws MatiereException, NoteException {
        List<Double> list = new ArrayList<Double>();
        list.add(15.0);
        list.add(15.0);
        list.add(15.0);
        e.ajouterNote("Math",15.0);
        e.ajouterNote("Math",15.0);
        e.ajouterNote("Math",15.0);
        e.moyenneMatiere("Math");
        assertEquals("La moyenne devrait etre egale a 15",list,e.getNote("Math"));
    }

    @Test(expected = MatiereException.class)
    public void moyenneMatiereInexistante() throws MatiereException, NoteException {
        List<Double> list = new ArrayList<Double>();
        list.add(15.0);
        list.add(15.0);
        list.add(15.0);
        e.ajouterNote("Math",15.0);
        e.ajouterNote("Math",15.0);
        e.ajouterNote("Math",15.0);
        e.moyenneMatiere("Physique");
    }

    @Test
    public void moyenneGenerale() throws NoteException, MatiereException {
        List<Double> list = new ArrayList<Double>();
        list.add(15.0);
        list.add(13.0);
        list.add(15.0);
        list.add(13.0);
        e.ajouterNote("Math",15.0);
        e.ajouterNote("Math",13.0);
        e.ajouterNote("Histoire",15.0);
        e.ajouterNote("Histoire",13.0);
        assertEquals("Devrait etre egale a 14",14.0,e.moyenneGenerale(),0.001);
    }

}