import java.util.*;

/**
 * Created by thomas on 27/09/16 at 09:05.
 */
public class Groupe{

    private Formation fm;
    private ArrayList<Etudiant> etu;

    public Groupe(Formation f){
        this.etu = new ArrayList<Etudiant>();
        fm = f;
    }

    public void ajouterEtudiant(Etudiant e) throws GroupeException{
        if(e.getFormation().equals(this.fm)) {
            if (!this.etu.contains(e))
                this.etu.add(e);
        }
        else
            throw new GroupeException();
    }

    public void supprimerEtudiant(Etudiant e) throws GroupeException{
        if(this.etu.contains(e))
            this.etu.remove(e);
        else
            throw new GroupeException();
    }

    public double calculerMoyenneMatiere(String mat) throws MatiereException{
        double res = 0;
        for(int i = 0; i<etu.size(); i++){
            res += etu.get(i).moyenneMatiere(mat);
        }
        return res = res / etu.size();
    }

    public double calculerMoyenneGeneraleGroupe() throws MatiereException {
        double res = 0;
        for(int i = 0; i<etu.size(); i++){
            res += etu.get(i).moyenneGenerale();
        }
        return res = res / etu.size();
    }

    public void triAlpha(){
        Collections.sort(etu, new Comparator<Etudiant>(){
            @Override
            public int compare(Etudiant e1, Etudiant e2){
                return e1.getIdentite().getNom().compareTo(e2.getIdentite().getNom());
            }
        });
    }

    public void triParMerite(){
        Collections.sort(etu, new Comparator<Etudiant>(){
            @Override
            public int compare(Etudiant e1, Etudiant e2){
                try {
                    return Double.compare(e1.moyenneGenerale(), e2.moyenneGenerale());
                } catch (MatiereException e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });
    }

    public ArrayList<Etudiant> getEtu(){
        return this.etu;
    }
}
