import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
/**
 * Created by thomas on 21/09/16 at 16:44.
 */
public class TestFormation {

    Formation f;
    @Before
    public void SetUp() throws MatiereException{
        f = new Formation();
        f.ajouterMatiere("Physique",1);
    }

    @Test
    public void test_supprimerBon() throws MatiereException{
        f.supprimerMatiere("Physique");
        assertEquals("Formation doit avoir aucune matiere",true,f.getEtu().isEmpty());
    }

    @Test(expected = MatiereException.class)
    public void test_supprimerPasBon() throws MatiereException{
        f.supprimerMatiere("Math");
    }

    @Test
    public void test_ajouterMatiereBon() throws MatiereException{
        f.ajouterMatiere("Math",1);
        assertEquals("Math doit avoir ete ajoute",true,f.getEtu().containsKey("Math"));
    }

    @Test(expected = MatiereException.class)
    public void test_ajouterMatierePasBon() throws MatiereException{
        f.ajouterMatiere("Physique",1);
    }
}
