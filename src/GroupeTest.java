import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by thomas on 02/10/16 at 19:31.
 */
public class GroupeTest {

    Groupe g;
    Formation f;
    Etudiant e;

    @Before
    public void setUp()throws MatiereException{
        f = new Formation();
        f.ajouterMatiere("Francais",1);
        f.ajouterMatiere("Math",2);
        g = new Groupe(f);
        e = new Etudiant("A","Thenot","Raphael",f);
    }

    @Test
    public void test_ajoutBon() throws GroupeException{
        g.ajouterEtudiant(e);
        assertEquals("Le groupe ne doit pas etre vide",true,!g.getEtu().isEmpty());
    }

    @Test(expected = GroupeException.class)
    public void test_ajoutPasBon() throws GroupeException, MatiereException{
        Formation f2 = new Formation();
        f2.ajouterMatiere("Math",2);
        g.ajouterEtudiant(new Etudiant("A","Thenot","Raphael",f2));
    }

    @Test
    public void test_SupprimerBon() throws GroupeException{
        g.ajouterEtudiant(e);
        g.supprimerEtudiant(e);
        assertEquals("g doit etre vide",true,g.getEtu().isEmpty());
    }

    @Test(expected = GroupeException.class)
    public void test_SupprimerPasBon() throws GroupeException{
        g.supprimerEtudiant(e);
    }

    @Test
    public void test_MoyenneMatBon()throws NoteException, GroupeException, MatiereException{
        g.ajouterEtudiant(e);
        e.ajouterNote("Francais",15.0);
        Etudiant e1 = new Etudiant("B","Lerat","Thomas",f);
        g.ajouterEtudiant(e1);
        e1.ajouterNote("Francais",13.0);
        assertEquals("La moyenne doit etre de 14",14.0,g.calculerMoyenneMatiere("Francais"),0.1);
    }

    @Test(expected = MatiereException.class)
    public void test_MoyenneMatPasBon()throws NoteException, GroupeException, MatiereException{
        g.ajouterEtudiant(e);
        e.ajouterNote("Francais",15.0);
        Etudiant e1 = new Etudiant("B","Lerat","Thomas",f);
        g.ajouterEtudiant(e1);
        e1.ajouterNote("Francais",13.0);
        g.calculerMoyenneMatiere("Math");
    }

    @Test
    public void test_MoyGeneBon()throws NoteException, GroupeException, MatiereException{
        g.ajouterEtudiant(e);
        e.ajouterNote("Francais",15.0);
        e.ajouterNote("Math", 12.0);
        Etudiant e1 = new Etudiant("B","Lerat","Thomas",f);
        g.ajouterEtudiant(e1);
        e1.ajouterNote("Math",16.0);
        e1.ajouterNote("Francais", 7.0);
        assertEquals("moyenne doit etre de 13",13.0,g.calculerMoyenneGeneraleGroupe(),0.1);
    }
}
